
var Router = require("express-promise-router");
const { users } = require("../../config/index.js");

const routes = Router()

// GET api/users/
routes.get("/", async (req, res, next) => {
    res.json(await users.getAll());
});

// GET api/users/123
routes.get("/:id", async (req, res) => {
    res.json(await users.findById(req.params.id));
});

// POST api/users/ {...}
routes.post("/", async (req, res) => {
    const draft = req.body;
    // req.app.emit('product:creating', draft)
    /// todo: validate 
    res.json(await users.create(draft));
});

// PUT api/users/123 {...}
routes.put("/:id", async (req, res) => {
    const draft = req.body;
    /// todo: validate 
    res.json(await users.update(draft));
});

// DELETE api/users/123
routes.delete("/:id", async (req, res) => {
    await users.delete(req.params.id)

    res.json({ deleted: req.params.id });
});


// GET api / users / 123 / posts
routes.get("/:id/posts", async (req, res) => {
    // posts.findBy...
    res.json({ id: '31', title: 'Test post' });
});


module.exports = routes