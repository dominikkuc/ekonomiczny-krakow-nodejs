var MongoClient = require("mongodb").MongoClient;

const client = new MongoClient("mongodb://localhost:27017/uniwersytet")

/**
 * Get connected mongo client
 * @returns {Promise<MongoClient>}
 */
module.exports.connect = () => {
    return client.connect();
}

// db.collection("users")
//     .find()
//     .toArray(function (err, result) {
//         if (err) throw err;

//         console.log(result);
//     });

module.exports = client;