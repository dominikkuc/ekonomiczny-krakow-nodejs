myInfiniteIterator = {
    value: 0,
    max: 100,
    [Symbol.iterator]: function () {
        return {
            next: () => {
                return { value: this.value++, done: this.max <= this.value };
            },
        };
    },
}

// it = myInfiniteIterator[Symbol.iterator]()
// it.next()

myInfiniteIterator.max = 10;

for (let i of myInfiniteIterator) {
    console.log(i);

    //     if(i> 10) break;
}