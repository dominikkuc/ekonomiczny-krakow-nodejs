const {
    Router
} = require("express");

const fs = require('fs')
const path = require('path')
const buffer = require('buffer')


const routes = Router()


routes.get('/generate/catalog', (req, res) => {
    // list folders
    // for each folder
    // read product.json
    // read description.md
    // write :
    // - product name
    // - product price
    // - description
    // - separator /r/n/r/n================/r/n/r/n
    // Save Catalog to /public/products-catalog.md

    const dirPath = './../../dane/products';

    fs.readdir(dirPath, {
        /* withFileTypes: true */ }, (err, resultDir) => {
        if (err) {
            return res.status(500).send(err)
        }

        let productNames = []

        resultDir.forEach(element => {
            const productFileName = path.resolve(dirPath + '/' + element, 'product.json')
            fs.readFile(productFileName, (err, data) => {
                let parsedData = JSON.parse(data)
                let product = {
                    'id': parsedData.id,
                    'name': parsedData.name,
                    'price': parsedData.price
                }
                const descriptionFileName = path.resolve(dirPath + '/' + element, 'description.md')
                fs.readFile(descriptionFileName, (err, data) => {
                    product.description = data.toString().substr(0, 20)
                    productNames.push(product)
                    if (productNames.length == resultDir.length) {
                        const resultFileName = path.resolve(dirPath, 'result.json')
                        fs.writeFile(resultFileName, JSON.stringify(productNames), (err, data) => {
                            res.json(productNames)
                        })
                    }
                })
            })




        });

    })
})

routes.get('/list/:directory?', (req, res) => {

    const dirPath = path.join('./public', req.params.directory || '');
    // const result = fs.readdirSync(path.join('./public', req.params.directory || ''));
    // res.json(result)

    fs.readdir(dirPath, {
        /* withFileTypes: true */ }, (err, result) => {
        if (err) {
            return res.status(500).send(err)
        }
        res.json(result)
    })

    /* 
    fetch('http://localhost:3000/files/',{})
    .then(res => res.json())
    .then(console.log)
*/
})

routes.delete('/:fileName', (req, res) => {

    const filePath = path.resolve('./public', req.params.fileName);
    fs.unlink(filePath, (err) => {
        if (err) {
            return res.status(500).send(err)
        }
        res.json({
            message: 'ok'
        })
    })
    /* 
        fetch('http://localhost:3000/files/text copy.txt',{
            method:'DELETE'
        })
        .then(res => res.json())
        .then(console.log)
    */
})


routes.get('/:fileName', (req, res) => {
    // TODO: send file
    // res.set('Content-Disposition', 'attachment; filename="filename.txt"')
    // const content = fs.readFileSync('./public/text.txt')
    const fileName = path.resolve(process.cwd(), 'public', req.params.fileName)

    // res.send(content)
    // res.attachment()
    res.download(fileName, fileName, {})
    // res.sendFile()
})


module.exports = routes