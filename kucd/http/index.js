const http = require('http')

console.log('Aplikacja');

const HOST = 'localhost';
const PORT = 8080

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/html");
    res.write('<html><body>')
    res.write('<h1>Elo world</h1>')
    res.end('</body></html>')
})

server.listen(PORT, HOST, () => {
    console.log('Server is Listening on: ' + HOST + ':' + PORT)
})