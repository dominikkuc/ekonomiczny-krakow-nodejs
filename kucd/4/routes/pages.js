var { Router } = require("express");

const routes = Router()

routes.use(function (req, res, next) {
    var now = new Date();
    console.log(`[${req.method}] - ${now.getHours()}:${now.getMinutes()}:${now.getSeconds()} - ${req.originalUrl}`)
    next()
})

routes.get("/", function (req, res) {
    res.render('pages/index', {
        user: req.user
    })
});

routes.get("/signin", function (req, res) {
    res.render('pages/signin')

});

routes.get("/signup", function (req, res) {
    res.render('pages/signup')
});

routes.get("/contact", function (req, res) {
    const { email = '', message = '' } = req.query
    res.render('pages/contact', { req, email, message })
});

// routes.post("/contact", function (req, res) {
//     const { email, message } = req.query
// })


module.exports = routes
