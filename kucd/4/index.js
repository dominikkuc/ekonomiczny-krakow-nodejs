require('dotenv').config({})
const express = require("express");
const config = require("./config");
const { admin, pages, api } = require("./routes");

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 3000;

var app = express();

config(app)

// TODO:
// Log requests [GET] - 12:44:00 - /pages 

app.use('/admin', admin)
app.use('/pages', pages)
app.use('/strony', pages)
app.use('/api', api)

app.get("/", function (req, res) {
    res.redirect('/pages/')
});

// Custom error handler middleware
// app.use((error, req, res, next) => {
//     res.render('layout/error')
// })

const server = app.listen(PORT, HOST, () => {
    console.log(`Server is Listening on http://${HOST}:${PORT}/`);
});
