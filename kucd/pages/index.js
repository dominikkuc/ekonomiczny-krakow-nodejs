var express = require("express");
const { admin, pages } = require("./routes");

const HOST = 'localhost';
const PORT = 3000;

var app = express();

app.set('view engine', 'ejs');
app.set('views', './views')
app.set('view options', {});


app.use('/admin', admin)
app.use('/pages', pages)
app.use('/strony', pages)


app.get("/", function (req, res) {
    res.redirect('/pages/')
});

app.get("/api", function (req, res) {
    res.status(200).send({ message: 'Hello API' });
});

const server = app.listen(PORT, HOST, () => {
    console.log(`Server is Listening on http://${HOST}:${PORT}/`);
});
