
/* TODO:
    - Home page html
    - Signin page html
    - Signup page html
    - Conctact page html
*/


var { Router } = require("express");

const routes = Router()


routes.get("/", function (req, res) {
    res.render('pages/index')
});

routes.get("/signin", function (req, res) {
    res.render('pages/signin')

});

routes.get("/signup", function (req, res) {
    res.render('pages/signup')
});

routes.get("/contact", function (req, res) {
    const { email = '', message = '' } = req.query
    res.render('pages/contact', { req, email, message })
});

routes.get("/aboutus", function (req, res) {
    res.render('pages/aboutus')
});

// routes.post("/contact", function (req, res) {
//     const { email, message } = req.query
// })


module.exports = routes
