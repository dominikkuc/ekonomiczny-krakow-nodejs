var express = require('express')
var app = express()

var x = 123;


app.get('/', function(req, res) {
    x = 345;
    res.status(200)
        .send("<h1>Homepage</h1>");
})

app.get('/admin', function(req, res) {
    res.status(200)
        .send(`<h1>Hello admin ${x}</h1>`);
})

app.get('/api', function(req, res) {
    res.status(200)
        .send({'message': 'Hello API'});
})

app.listen(8080)