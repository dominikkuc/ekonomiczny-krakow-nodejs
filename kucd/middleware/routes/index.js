const admin = require("./admin");

/* Module Barrel */

module.exports.admin = admin
module.exports.pages = require("./pages");
module.exports.api = require("./api");
