const { Router } = require("express");

const routes = Router()


routes.get("/", function (req, res) {
    res.json({ message: 'Hello API' });
});


routes.post("/users/signin", function (req, res) {
    const { email, password } = req.body
    
    let errors = []

    if (!email || !password) {
        errors.push('Invalid data')
    }

    if (errors.length) {
        res.status(400).json({ errors })
        return
    }

    res.json({ message: 'Success', user: req.body });
});

routes.post("/users/signup", function (req, res) {
    const { email, password } = req.body
    
    let errors = []

    if (!email || !password) {
        errors.push('Invalid data')
    }
    if (errors.length) {
        res.status(400).json({ errors })
        return
    }

    res.json({ message: 'Success', user: req.body });
});

module.exports = routes