
module.exports = function (app) {

    app.set('view engine', 'ejs');
    app.set('views', './views')
    // https://ejs.co/#docs
    app.set('view options', {});

}
